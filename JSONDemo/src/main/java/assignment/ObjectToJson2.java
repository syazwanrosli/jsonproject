package assignment;

import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ObjectToJson2 {

	public static void main(String[] args) {
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Object obj = createmyObject();
		System.out.println(obj); // normal java printed
		String k = gson.toJson(obj); // convert java to JSON
		System.out.println(k);
		
		try(FileWriter writer = new FileWriter("C:\\JSONproject\\jsonfile.json")){
			gson.toJson(obj, writer);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		
	}

	private static Object createmyObject() {
		Object obj = new Object();
		obj.setName("Muhammad Syazwan");
		obj.setAge(25);
		obj.setGender("Male");
		obj.setCity("Kuala Lumpur");
		obj.setCountry("Malaysia");
		
		return obj;
	}

}