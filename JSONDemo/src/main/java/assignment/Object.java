package assignment;

public class Object {
	private String Name, City, Country, Gender;
	private int Age;
	
	
	public String getName() {
		return Name;
	}
	public void setName(String Name) {
		this.Name = Name;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		this.City = city;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		this.Country = country;
	}
	public String getGender() {
		return Gender;
	}
	public void setGender(String gender) {
		this.Gender = gender;
	}
	public int getAge() {
		return Age;
	}
	public void setAge(int age) {
		this.Age = age;
	}
	@Override
	public String toString() {
		return "Object [Name=" + Name + ", City=" + City + ", Country=" + Country + ", Gender=" + Gender + ", Age="
				+ Age + "]";
	}
	

	
	
	

}
