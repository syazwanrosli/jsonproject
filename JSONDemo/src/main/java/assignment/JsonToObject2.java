package assignment;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import com.google.gson.Gson;

public class JsonToObject2 {

	public static void main(String[] args) {
			
		Gson gson = new Gson();
		
		try(Reader reader = new FileReader("C:\\JSONproject\\jsonfile.json")) {
			Object obj =  gson.fromJson(reader, Object.class);
			System.out.println(obj);
		}
		catch(IOException e) {
		e.printStackTrace();
		}
	}
}
