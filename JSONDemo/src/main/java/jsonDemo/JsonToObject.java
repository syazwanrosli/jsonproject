package jsonDemo;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import com.google.gson.Gson;

// convert JSON to java Object
public class JsonToObject {

	public static void main(String[] args) {
			Gson gson = new Gson();
			
			try(Reader read = new FileReader("C:\\JSONproject\\myfile.json")){
				empDetails ed = gson.fromJson(read, empDetails.class);
				System.out.println(ed);
			}
			catch(IOException e) {
				e.printStackTrace();
			}
	}
}
