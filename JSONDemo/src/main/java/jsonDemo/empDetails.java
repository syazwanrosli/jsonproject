package jsonDemo;

public class empDetails {
	private String Name;
	private int	Age;
	private String City;
	
	public String getName() {
		return Name;
	}
	public void setName(String Name) {
		this.Name = Name;
	}
	public int getAge() {
		return Age;
	}
	public void setAge(int Age) {
		this.Age = Age;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String City) {
		this.City = City;
	}
	public String toString() {
		
		return "empDetails [name="+Name+", age="+Age+", city="+City+"]";
		
	}
	
}
