package jsonDemo;

import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
// writing JSON
public class ObjectToJson {
	
	public static void main(String[] args) {
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		empDetails ed = createmyObject();
		System.out.println(ed); // java object printed
		String j = gson.toJson(ed); // to convert java to JSON
		System.out.println(j);
		
		try(FileWriter writer = new FileWriter("C:\\JSONproject\\myfile.json")){
			gson.toJson(ed, writer);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}

	private static empDetails createmyObject() {
		empDetails ed = new empDetails();
		ed.setName("Syazwan Rosli");
		ed.setAge(25);
		ed.setCity("Kuala Lumpur");
		
		return ed;
	}
}
